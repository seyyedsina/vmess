package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/joho/godotenv"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	cors "github.com/itsjamie/gin-cors"
)

var (
	VmessPort       int
	VmessSSLPort    int
	VmessHost       string
	NginxConfDir    string
	NginxSSLConfDir string
	Production      bool
)

func init() {
	if err := godotenv.Load(".env"); err != nil {
		log.Fatalln(err)
	}

	VmessPort, _ = strconv.Atoi(os.Getenv("vmess_port"))
	VmessSSLPort, _ = strconv.Atoi(os.Getenv("vmess_ssl_port"))
	VmessHost = os.Getenv("vmess_host")
	NginxConfDir = os.Getenv("nginx_conf_dir")
	NginxSSLConfDir = os.Getenv("nginx_ssl_conf_dir")
	Production, _ = strconv.ParseBool(os.Getenv("production"))
}

func main() {
	r := gin.New()
	r.HandleMethodNotAllowed = true

	r.Use(cors.Middleware(cors.Config{
		Origins:         "*",
		Methods:         "GET, PUT, POST, PATCH, DELETE",
		RequestHeaders:  "*",
		ExposedHeaders:  "",
		MaxAge:          50 * time.Second,
		ValidateHeaders: false,
	}))

	r.Static("/assets", "./assets")
	r.LoadHTMLGlob("./templates/*.gohtml")
	r.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.gohtml", gin.H{})
	})

	r.POST("/add-token", func(c *gin.Context) {
		var req struct {
			Tokens []string `json:"tokens"`
			SSL    bool     `json:"ssl"`
		}
		if err := c.BindJSON(&req); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		res := make([]string, 0)
		for _, v := range req.Tokens {
			token, err := decodeVmess(v)
			if err != nil {
				c.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
				return
			}
			oldPort := token.Port
			token.Host = VmessHost
			token.Add = VmessHost
			if req.SSL {
				token.Port = VmessSSLPort
				token.Tls = "tls"
			} else {
				token.Port = VmessPort
			}

			t, err := encodeVmess(token)
			if err != nil {
				c.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
				return
			}
			if Production {
				err = addNginxConf(token.Path, oldPort, req.SSL)
				if err != nil {
					c.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
					return
				}
			}

			res = append(res, t)
		}
		if len(res) > 0 && Production {
			err := restartNginx()
			if err != nil {
				c.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
				return
			}
		}
		c.JSON(http.StatusOK, gin.H{"tokens": res})
	})

	if err := r.Run(":3004"); err != nil {
		log.Fatalln(err)
	}

}

const nginxConf = `location %s {
        proxy_pass http://localhost:%d;
        proxy_redirect off;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $connection_upgrade;
        proxy_set_header Host $http_host;
}
`

func addNginxConf(path string, port int, ssl bool) error {
	if _, err := os.Stat(NginxConfDir); os.IsNotExist(err) {
		if err := os.MkdirAll(NginxConfDir, os.ModePerm); err != nil {
			return err
		}
	}

	confDir := NginxConfDir
	if ssl {
		confDir = NginxSSLConfDir
	}
	fName := fmt.Sprintf("%s/%s.conf", confDir, path)
	f, err := os.OpenFile(fName, os.O_RDWR|os.O_CREATE, 0777)
	if err != nil {
		return err
	}
	defer f.Close()

	err = os.WriteFile(fName, []byte(fmt.Sprintf(nginxConf, path, port)), 0777)
	if err != nil {
		return err
	}
	return nil
}

func restartNginx() error {
	cmd := exec.Command("systemctl",
		"restart",
		"nginx",
	)

	_, err := cmd.Output()
	if err != nil {
		return err
	}

	return nil
}

const (
	VmessTag string = "vmess://"
)

func decodeVmess(token string) (*vmess, error) {
	l := strings.Split(token, VmessTag)
	if len(l) != 2 {
		return nil, fmt.Errorf("%s", "توکن اشتباه است")
	}
	t1, err := base64.StdEncoding.DecodeString(l[1])
	if err != nil {
		return nil, fmt.Errorf("%s", "فرمت توکن اشتباه است")
	}
	var v vmess
	err = json.Unmarshal(t1, &v)
	if err != nil {
		return nil, fmt.Errorf("%s", "ساختار توکن اشتباه است")
	}
	return &v, nil
}

func encodeVmess(v *vmess) (string, error) {
	d, err := json.MarshalIndent(v, "", "  ")
	if err != nil {
		log.Println("json marshal err: ", err)
		return "", fmt.Errorf("%s", "در ایجاد ساختار توکن مشکلی بوجود آمد")
	}
	d2 := base64.StdEncoding.EncodeToString(d)
	return fmt.Sprintf("%s%s", VmessTag, d2), nil
}

type vmess struct {
	V    string `json:"v"`
	Ps   string `json:"ps"`
	Add  string `json:"add"`
	Port int    `json:"port"`
	Id   string `json:"id"`
	Aid  int    `json:"aid"`
	Net  string `json:"net"`
	Type string `json:"type"`
	Host string `json:"host"`
	Path string `json:"path"`
	Tls  string `json:"tls"`
}
